#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2010 Maurizio Porrato <maurizio.porrato@gmail.com>
# See LICENSE.txt for copyright info

from twisted.internet import reactor
from frn.protocol.manager import FRNManagerServer, FRNManagerServerFactory
from twisted.enterprise.adbapi import ConnectionPool
from frn.manager.dummy import DummyManager
from frn.manager.remote import RemoteManager
from frn.manager.database import DatabaseManager
from frn.user import FRNUser
from twisted.python import log

if __name__ == '__main__':
    import sys

    log.startLogging(sys.stderr)

    def dummyManagerFactory():
        log.msg("Building DummyManager")
        return DummyManager()

    def remoteManagerFactory():
        log.msg("Building RemoteManager")
        return RemoteManager(reactor)

    pool = ConnectionPool("sqlite3", "/dev/shm/frnmanager.sqlite3", check_same_thread=False, cp_noisy=True)

    def databaseManagerFactory():
        log.msg("Building DatabaseManager")
        return DatabaseManager(pool)

    reactor.listenTCP(10025, FRNManagerServerFactory(
        databaseManagerFactory
    ))
    reactor.run()

# vim: set et ai sw=4 ts=4 sts=4:
