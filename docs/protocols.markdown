The FreeRadioNetwork protocol
=============================

This document describes the results of reverse-engeneering the official
set of [FreeRadioNetwork](http://freeradionetwork.eu) programs through
network traffic sniffing and software decompilation.


System components
-----------------

The FreeRadioNetwork system is composed of three programs:

* client
* server
* system manager

Client and server executables are freely available, while the system
manager is not. The role of the system manager is account management,
including password generation and credentials checking, keeping track
of active servers and connected users.

The client talks to the system manager only to get the list of active
servers and to request the creation of a new account/password: any other
communication from the client goes to the server.

Server listens for clients connection on TCP port 10024 (by default, but
can be changed through the server GUI), while the system manager listen
for server connections and client queries on TCP port 10025. In both the
client and the server, the system manager address is hard coded as
`frn.no-ip.info`.


Client - server protocol
------------------------

The client sends requests to the server in the form of CR+LF terminated
ascii text lines. Response to commands and unsolicited events from the
server (i.e. incoming text messages), excluding authentication procedure
and voice packets, starts with a byte indicating the message type
followed by one CR+LF terminated ascii line containing the decimal count
of lines following and then the lines composing the message body.


### Authentication ###

As soon as the TCP connection is established, the client sends a line
containing user's info and authentication credential formatted enclosing
fields between html-like tags, for example:

    CT:<VX>2010002</VX><EA>user@example.com</EA><PW>XYZJKWXY</PW><ON>CALLSIGN, Name</ON><BC>PC Only</BC><DS></DS><NN>Country</NN><CT>City - Locator</CT><NT>Network</NT>

The line is, as usual, followed by the CR and LF chars (ascii 13 an 10).
The meaning of the fields is the following:

* **VX**: Client version (four digits for the year and three digits for
the release number)
* **EA**: User's email address
* **PW**: The password assigned by the system manager to the user
* **ON**: Operator's name, usually in the form CALLSIGN, RealName
* **BC**: Indicates the client type:
 * `PC Only`
 * `Parrot`
 * `Crosslink`
 * *BBB* `Ch`*CCC* *MM* `CTC`*TT* where **BBB** is the band (3 digits:
 433, 446, 027,...), **CCC** is the channel number (3 digits), **MM**
 is the modulation (AM or FM), **TT** is the subtone number (2 digits)
* **DS**: Description, only used for gateways, empty for other client
types
* **NN**: Country
* **CT**: Client's location in the form of City - Locator
* **NT**: Network (channel, room) to join on login

In response to the authentication request, the server sends two lines of
text (CR+LF terminated):

    2010002
    <MT></MT><SV>2009005</SV><AL>OK</AL><BN>backup.server.org</BN><BP>10024</BP><KP>327119</KP>

The first line contains the latest client version available.
The second one contains authentication results and server info:

* **MT**: Unknown meaning, always empty
* **SV**: Server version
* **AL**: Authentication result:
 * `OK`: Success, unprivileged user
 * `ADMIN`: Success, user has administration privileges
 * `OWNER`: Success, user is the server owner
 * `NOK`: Failed, invalid client version
 * `WRONG`: Failed, invalid credentials
 * `BLOCK`: Failed, user already logged in
* **BN**: Backup server
* **BP**: Backup server port
* **KP**: Six digits code used for second phase

At this point, if authentication succeeds, server versions greater than
2009004 needs one more line from the client before considering the
connection fully established. The line is a five digits number computed
as follows:

1. Split the KP code in two-digits numbers: `AABBCC`
2. Let `X` be the result of: `(AA+2)*(BB+1)+(CC+4)*(CC+7)`
3. Extend `X` to be 5 digits long, padding to the left with zeroes as
needed and let's call the resulting digits `DEFGH` respectively
4. The authentication code is composed by rearranging the digits as
`GDFHE`

For example, let's KP be `327119`. AA would be 32, BB=71 and CC=19.

    X = (32+2)*(71+1)+(19+4)*(19+7) = 3046

Padding to five digits we would get `03046`, so D=0, E=3, F=0, G=4, H=6
so the auth code wold be `40063`.


### Keepalive ###

After the communication between client and server has been established,
server starts sending keepalive messages to the client at a rate of two
messages per second.
The keepalive message, as any other server to client message must be
acknowledged by the client.

Server to client:

* `chr(0)`: keepalive message identifier

Client to server:

* `P\r\n`


### Sending audio signal ###

Sending audio signal from client to server is a two-step procedure:
client must request permission to trasmit to the server and then, if
server acknowledges, client can start transmitting audio data.

Client to server:

* `TX0\r\n`

Server to client:

* `chr(1)chr(hh)chr(ll)`: transmission acknowledged (hh*256+ll is the
client index in client list)

Client to server:

* `TX1\r\n` followed by 325 bytes: data following the first line
contains 10 GSM 6.10 frames (representing 20mS of audio signal each one)
encoded with WAV#49 variant.

Client to server:

* `RX0\r\n`: client stops transmitting audio.


### Receiving audio signal ###

Server to client:

* `chr(2)chr(hh)chr(ll)` followed by 325 bytes: data following the first
3 bytes contains 10 GSM 6.10 frames (representing 20mS of audio signal
each one) encoded with WAV#49 variant. hh*256+ll represents the index in
the client list of the client sending audio.


### Sending text messages ###

Client to server:

* `TM:<ID>clientId</ID><MS>Message body</MS>\r\n`: Sends "Message body"
to client having id "clientId". If clientId is an empty string,
message is sent to all clients on the same network, including the
message sender.


### Receiving text messages ###

Server to client:

* `chr(4)` followed by 3-lines message: incoming message first line
contains the sender ID, second line the message body, third line the
message type: "A" for network messages or "P" for messages directed to a
single client.


### Receiving client list ###

Server to client:

* `chr(3)` followed by a multiline message: each line contains a client
description in an XML-like format containing the following fields:

 * **S**: Client status: 0=Available, 1=Not available, 2=Absent
 * **M**: Client mute status: 0=Not muted, 1=Muted
 * **NN**: Same as in authetication procedure
 * **CT**: Same as in authetication procedure
 * **BC**: Same as in authetication procedure
 * **ON**: Same as in authetication procedure
 * **ID**: Client ID (assigned to the client by the system manager
through the server during the authentication phase)
 * **DS**: Same as in authetication procedure


### Receiving network list ###

Server to client:

* `chr(5)` followed by a multiline message: each line contains the name
of a network.


