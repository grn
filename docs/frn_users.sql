CREATE TABLE frn_users (
-- Fields required for FRN compatibility
    "id" INTEGER PRIMARY KEY AUTOINCREMENT, -- User ID
    "on" VARCHAR(30), -- Operator name (Callsign, Name)
    "ea" VARCHAR(30), -- Email address
    "pw" VARCHAR(30), -- Password
    "bc" VARCHAR(30), -- Band/channel
    "ds" VARCHAR(30), -- Description
    "ct" VARCHAR(30), -- City - Locator
    "nn" VARCHAR(30), -- Country
    "nt" VARCHAR(30), -- Network
    "ip" VARCHAR(15), -- Client IP
-- GRN specific fields
    "rip" VARCHAR(20), -- IP the account was registered from
    "cre" TIMESTAMP NOT NULL -- Date/time of account creation
        DEFAULT CURRENT_TIMESTAMP,
    "act" TIMESTAMP, -- Date/time of last account activity
    "srv" VARCHAR(30) -- Server:port where usere is actually logged
);

CREATE UNIQUE INDEX idx_frn_users_on_ea ON frn_users("on","ea");

-- vim: set et ai sw=4 ts=4 sts=4:
