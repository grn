#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <gsm.h>

int main() {
    gsm handle = gsm_create();
    int one  = 1;
    gsm_option(handle, GSM_OPT_WAV49, &one);
    unsigned char src610[65];
    gsm_signal dst[160];
    while (!feof(stdin)) {
        int rv = fread(src610, 65, 1, stdin);
        if (rv < 0) {
            fprintf(stderr, "%s\n", strerror(errno));
            return -1;
        }
        else if (rv == 0) {
            return 0;
        }
        int frameIndex = 0;
        for (frameIndex = 0; frameIndex <= 1; frameIndex++) {
            gsm_decode(handle, src610 + 33 * frameIndex, dst);
            rv = fwrite(dst, sizeof dst, 1, stdout);
            if (rv < 0) {
                fprintf(stderr, "%s\n", strerror(errno));
                return -1;
            }
        }
    }
    gsm_destroy(handle);

    return EXIT_SUCCESS;
}
