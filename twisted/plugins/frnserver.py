# -*- coding: utf-8 -*-
#
# Copyright 2010 Maurizio Porrato <maurizio.porrato@gmail.com>
# See LICENSE.txt for copyright info

from zope.interface import implements
from twisted.plugin import IPlugin
from twisted.application.service import IServiceMaker
from twisted.application import internet
from frn.protocol.server import FRNServer, FRNServerFactory
from frn.manager.remote import RemoteManager
from frn.user import FRNUser

from os.path import curdir, join as pjoin
from ConfigParser import ConfigParser
from twisted.python import usage


class Options(usage.Options):

    optParameters = [
        ["confdir", "c", curdir, "Directory containing config files"]
    ]

    def parseArgs(self, serverdef):
        account_name, server_name = serverdef.split(':', 1)

        basedir = self['confdir']
        acfg = ConfigParser()
        acfg.read(['/etc/grn/accounts.conf',
            pjoin(basedir,'accounts.conf'), 'accounts.conf'])

        scfg = ConfigParser()
        scfg.read(['/etc/grn/servers.conf',
            pjoin(basedir,'servers.conf'), 'servers.conf'])

        self['server'] = scfg.get(server_name, 'server')
        self['port'] = scfg.getint(server_name, 'port')
        self['backup_server'] = scfg.get(server_name, 'backup_server')
        self['backup_port'] = scfg.getint(server_name, 'backup_port')
        self['owner'] = acfg.get(account_name, 'email')
        self['password'] = acfg.get(account_name, 'password')


class FRNServerServiceMaker(object):
    implements(IServiceMaker, IPlugin)
    tapname = "frnserver"
    description = "Freeradionetwork server"
    options = Options

    def makeService(self, options):
        return internet.TCPServer(options['port'],
            FRNServerFactory(
                pjoin(options['confdir'], 'tracker.shelve'),
#                RemoteManager(), # Authenticate on frn.no-ip.info
                RemoteManager(server='fri.no-ip.info', maskParrot=False),
                FRNUser(
                    SN=options['server'],PT=options['port'],
                    BN=options['backup_server'], BP=options['backup_port'],
                    OW=options['owner'],PW=options['password'])
            ))

serviceMaker = FRNServerServiceMaker()

# vim: set et ai sw=4 ts=4 sts=4:
