# -*- coding: utf-8 -*-
#
# Copyright 2010 Maurizio Porrato <maurizio.porrato@gmail.com>
# See LICENSE.txt for copyright info

from frn.utility import parseSimpleXML, formatSimpleXML


class FRNUser(object):

    def __init__(self, **kw):
        self._fields = {}
        self.update(**kw)

    def __getattr__(self, attr):
        if attr.startswith('_'):
            return super(FRNUser, self).__getattr__(attr)
        else:
            return self.get(attr)

    def __setattr__(self, attr, value):
        if attr.startswith('_'):
            super(FRNUser, self).__setattr__(attr, value)
        else:
            self.set(attr, value)

    def __str__(self):
        return self.asXML()

    def __repr__(self):
        return "FRNUser(%s)" % \
            ', '.join(["%s='%s'" % (k,v) for k,v in self.items()])

    def set(self, field, value):
        self._fields[field.lower()] = str(value)

    def get(self, field, default=''):
        return self._fields.get(field.lower(),'')

    def items(self, *fields):
        if len(fields) == 0:
            fields = self._fields.keys()
        r = []
        for field in fields:
            r.append((field.upper(), self.get(field)))
        return r

    def dict(self, *fields):
        return dict(self.items(*fields))

    def update(self, **kw):
        for field, value in kw.items():
            self.set(field, value)

    def copy(self, **kw):
        n = FRNUser(**self._fields)
        n.update(**kw)
        return n

    def updateXML(self, xml):
        self.update(dict(parseSimpleXML(xml)))

    def asXML(self, *fields):
        return formatSimpleXML(self.items(*fields))


# vim: set et ai sw=4 ts=4 sts=4:
