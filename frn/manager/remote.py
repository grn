# -*- coding: utf-8 -*-
#
# Copyright 2010 Maurizio Porrato <maurizio.porrato@gmail.com>
# See LICENSE.txt for copyright info

from zope.interface import implements
from twisted.internet.defer import Deferred, succeed
from frn.manager import IManager
from twisted.python import log
from frn.protocol.manager import FRNManagerClient, FRNManagerClientFactory


class CustomManagerClientFactory(FRNManagerClientFactory):

    def __init__(self, user):
        self.user = user
        self.authResult = False
        self.client = None
        self.authDone = False
        self.deferred = Deferred()

    def managerConnected(self, connection):
        def authReply(auth):
            self.resetDelay()
            self.authResult = auth
            self.authDone = (auth['AL'] == '0')
            if self.authDone:
                self.deferred.callback(auth)
        connection.sendServerLogin(self.user).addCallback(authReply)

    def managerDisconnected(self, connection):
        pass

    def buildProtocol(self, addr):
        p = FRNManagerClientFactory.buildProtocol(self, addr)
        self.client = p
        return p


class RemoteManager(object):

    implements(IManager)

    def __init__(self, server='frn.no-ip.info', port=10025, maskParrot=True, reactor=None):
        if reactor is None:
            from twisted.internet import reactor
            self.reactor = reactor
        else:
            self.reactor = reactor
        self.server = server
        self.port = port
        self.maskParrot = maskParrot
        self.factory = None

    def doConnect(self):
        self.reactor.connectTCP(self.server, self.port, self.factory)

    def serverLogin(self, server):
        self.factory = CustomManagerClientFactory(server)
        self.doConnect()
        return self.factory.deferred

    def serverLogout(self, server):
        if self.factory.client is not None:
            return self.factory.client.sendServerLogout(server)

    def clientLogin(self, server, user):
        if self.maskParrot and user.BC == 'Parrot':
            u = user.copy(BC='PC Only')
        else:
            u = user.copy()
        return self.factory.client.sendClientLogin(u)

    def clientLogout(self, server, user):
        if self.maskParrot and user.BC == 'Parrot':
            u = user.copy(BC='PC Only')
        else:
            u = user.copy()
        return self.factory.client.sendClientLogout(u)

    def getClientList(self):
        return self.factory.client.getClientList()

    def registerUser(self, user):
        return self.factory.client.registerUser(user)


# vim: set et ai sw=4 ts=4 sts=4:
