# -*- coding: utf-8 -*-
#
# Copyright 2010 Maurizio Porrato <maurizio.porrato@gmail.com>
# See LICENSE.txt for copyright info

from zope.interface import implements
from frn.manager import IManager
from twisted.internet import defer
from random import randint


class DummyManager(object):

    implements(IManager)

    def _randId(self):
        return '.'.join([str(randint(1,254)) for i in range(4)])

    def serverLogin(self, server):
        return defer.succeed(0)

    def serverLogout(self, server):
        return defer.succeed(None)

    def clientLogin(self, server, user):
        return defer.succeed(self._randId())

    def clientLogout(self, server, user):
        return defer.succeed('OK')

    def getClientList(self):
        return defer.succeed({})

    def registerUser(self, user):
        return defer.succeed('OK')


# vim: set et ai sw=4 ts=4 sts=4:
