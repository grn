# -*- coding: utf-8 -*-
#
# Copyright 2010 Maurizio Porrato <maurizio.porrato@gmail.com>
# See LICENSE.txt for copyright info

from zope.interface import Interface


class IManager(Interface):

    def serverLogin(server):
        """Logs server on"""

    def serverLogout(server):
        """Logs server out"""

    def clientLogin(server, user):
        """Logs client in"""

    def clientLogout(server, user):
        """Logs client out"""

    def getClientList():
        """Lists logged in clients"""

    def registerUser(user):
        """Registers new user"""


# vim: set et ai sw=4 ts=4 sts=4:
