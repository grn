# -*- coding: utf-8 -*-
#
# Copyright 2010 Maurizio Porrato <maurizio.porrato@gmail.com>
# See LICENSE.txt for copyright info

from twisted.protocols.basic import LineReceiver


class BufferingLineReceiver(LineReceiver):

    def connectionMade(self):
        self.rawBuffer = ""
        self.rawExpected = None
        LineReceiver.connectionMade(self)

    def expectRawData(self, howmany):
        self.setRawMode()
        self.rawExpected = howmany
        self.rawDataReceived('')

    def rawDataReceived(self, data):
        if self.rawExpected is None:
            raise NotImplementedError
        self.rawBuffer += data
        if len(self.rawBuffer) >= self.rawExpected:
            expected = self.rawBuffer[:self.rawExpected]
            rest = self.rawBuffer[self.rawExpected:]
            self.rawBuffer = ""
            self.rawExpected = None
            self.expectedReceived(expected)
            self.dataReceived(rest)

    def setLineMode(self, extra=""):
        LineReceiver.setLineMode(self, extra+self.rawBuffer)

    def expectedReceived(self, data):
        raise NotImplementedError


# vim: set et ai sw=4 ts=4 sts=4:
