# -*- coding: utf-8 -*-
#
# Copyright 2010 Maurizio Porrato <maurizio.porrato@gmail.com>
# See LICENSE.txt for copyright info

import re
from random import choice


def responseToChallange(kp):
	if len(kp) != 6:
		return 'ERROR'
	aa, bb, cc = int(kp[:2]), int(kp[2:4]), int(kp[4:6])
	defgh = "%05d" % ((cc+7)*(cc+4)+(bb+1)*(aa+2), )
	return defgh[3]+defgh[0]+defgh[2]+defgh[4]+defgh[1]


def makeRandomChallange():
    return ''.join([choice('0123456789') for i in range(6)])


re_tag = re.compile(r"<([A-Z]{1,2})>(.*)</\1>")

def parseSimpleXML(xml):
    return dict(re_tag.findall(xml))


def formatSimpleXML(elements):
    if hasattr(elements, 'items'):
        items = elements.items()
    else:
        items = elements
    return ''.join(["<%s>%s</%s>" % (k.upper().encode('utf8'),unicode(v).encode('utf8'),k.upper().encode('utf8')) for k,v in items])

# vim: set et ai sw=4 ts=4 sts=4:
